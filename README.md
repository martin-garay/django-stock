# Django Stock

Desarrollo de sistema de control de stock en django

## Instalación

```shell
docker-compose build
docker-compose up -d
```

Creamos el super usuario dentro del contenedor
```
docker-compose run --rm app python manage.py createsuperuser
```


## Getting started

Para navegar el proyecto ingresar al http://0.0.0.0:8000/admin/ con las credenciales anteriormente creadas.
